function rgb2hsv(r, g, b)
	local r = r / 255
	local g = g / 255
	local b = b / 255

	local max = math.max(r, g, b)
	local min = math.min(r, b, b)
	local dlt = max - min

	local hue = 0
  local sat = 0
  local val = max

  if dlt == 0 then return hue, sat, val end
  if max >= 0 then sat = dlt / max end

	if max == r then hue = (g - b) / dlt % 6 end
	if max == g then hue = (b - r) / dlt + 2 end
	if max == b then hue = (r - g) / dlt + 4 end

  return (60 * hue), sat, val
end

function hsv2rgb(h, s, v)
  local r = 0
  local g = 0
  local b = 0

  local x = h / 60
  local i = math.floor(x)
  local m = i % 6
  local f = x - i
  local p = v * (1 - s)
  local q = v * (1 - f * s)
  local t = v * (1 - (1 - f) * s)

  if m == 0 then r, g, b = v, t, p end
  if m == 1 then r, g, b = q, v, p end
  if m == 2 then r, g, b = p, v, t end
  if m == 3 then r, g, b = p, q, v end
  if m == 4 then r, g, b = t, p, v end
  if m == 5 then r, g, b = v, p, q end

  r = math.floor((r * 255) + 0.5)
  g = math.floor((g * 255) + 0.5)
  b = math.floor((b * 255) + 0.5)

  return r, g, b
end