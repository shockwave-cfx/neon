local DEFAULT_SPEED = 900
local DEFAULT_COLOR = { 255, 255, 255 }

-------------------------------------------------------------------------------

local vehicles = setmetatable({}, {
  __index = function(t, k, v)
    local v = {
      isPulsing = false,
      speed = DEFAULT_SPEED,
      color = { table.unpack(DEFAULT_COLOR) }
    }

    rawset(t, k, v)
    return t[k]
  end
})

-------------------------------------------------------------------------------

function IsVehicleNeonPulsing(vehicle)
  local data = rawget(vehicles, vehicle)
  return data and data.isPulsing or false
end

function SetVehicleNeonPulsing(vehicle, isPulsing)
  vehicles[vehicle].isPulsing = isPulsing
end

function GetVehicleNeonPulseSpeed(vehicle)
  local data = rawget(vehicles, vehicle)
  return data and data.speed or DEFAULT_SPEED
end

function SetVehicleNeonPulseSpeed(vehicle, speed)
  vehicles[vehicle].speed = speed
end

function GetVehicleNeonColor(vehicle)
  local data = rawget(vehicles, vehicle)
  return data and table.unpack(data.color) or table.unpack(DEFAULT_COLOR)
end

function SetVehicleNeonColor(vehicle, r, g, b)
  vehicles[vehicle].color = { r, g, b }
end

-------------------------------------------------------------------------------

Citizen.CreateThread(function ()
  local function DarkenColor(color, darkness)
    local r, g, b = table.unpack(color)
    local h, s, v = rgb2hsv(r, g, b)
    return hsv2rgb(h, s, v * darkness)
  end

  local function ApplyNeonPulse(vehicle)
    if NetworkGetEntityOwner(vehicle) ~= PlayerId() then
      return false
    end

    local color = vehicles[vehicle].color
    local speed = vehicles[vehicle].speed

    -- Add vehicle ID to time for fake randomization between different vehicles.
    local time = GetGameTimer() + vehicle
    local sine = math.sin((math.pi / 2) * (time % speed) / (speed / 2))

    -- Darken the color to reduce neon brightness.
    local r, g, b = DarkenColor(color, sine)
    return SetVehicleNeonLightsColour(vehicle, r, g, b)
  end

  local function NeonLoop()
    for vehicle in pairs(vehicles) do
      ApplyNeonPulse(vehicle)
    end
  end

  while true do
    NeonLoop()
    Citizen.Wait(0)
  end
end)