Fivem Neon
==========

Simple FiveM script for custom neon options such as pulse neon, inspired by NFSU2.

Exports
-------

```lua
bool isPulsing = exports.neon:IsVehicleNeonPulsing(int vehicle)
```

```lua
void exports.neon:SetVehicleNeonPulsing(int vehicle, bool isPulsing)
```


```lua
int speed = exports.neon:GetVehicleNeonPulseSpeed(int vehicle)
```

```lua
void exports.neon:SetVehicleNeonPulseSpeed(int vehicle, int speed)
```


```lua
int r, int g, int b = exports.neon:GetVehicleNeonColor(int vehicle)
```

```lua
void exports.neon:SetVehicleNeonColor(int vehicle, int r, int g, int b)
```


