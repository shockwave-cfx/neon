resource_manifest_version '44febabe-d386-4d18-afbe-5e627f4af937'

client_script 'client/utils.lua'
client_script 'client/main.lua'

export 'IsVehicleNeonPulsing'
export 'SetVehicleNeonPulsing'

export 'GetVehicleNeonPulseSpeed'
export 'SetVehicleNeonPulseSpeed'

export 'GetVehicleNeonColor'
export 'SetVehicleNeonColor'